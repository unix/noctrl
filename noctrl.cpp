/* Simple daemon for switching keyboard layouts and disabling shortcuts.
 *
 * Currently catches everything with a Control or Alt, allowing only some
 * editing shortcuts (cursor movement, selection, clipboard, undo/redo, zoom,
 * page search) and Alt+F4 to exit Google Chrome. Certain additional keys are
 * disabled (F-keys, Menu). Mouse buttons are limited (no Shift+click to open a
 * new window).
 *
 * This needs to start pretty soon in the session and probably clashes with
 * window managers or desktop environment's settings daemons, because it grabs
 * basically all (Ctrl- or Alt-modified) keys on the X root window.
 *
 * All configuration is done in compile time. Change the definitions below to
 * edit allowed shortcuts and disabled keys. The shortcuts are defined by a
 * modifier mask and a keysym (not a keycode!). The "first" keysym is always
 * considered (e.g., the lowercase letter), even when Shift or Mod3 is pressed.
 *
 * Author: Adam Matousek <xmatous3@fi.muni.cz>, 2022
 */

#include <set>
#include <vector>
#include <string>
#include <iostream>
#include <sys/wait.h>

extern "C" {
#include <X11/Xlib.h>
#include <X11/Xutil.h>
}

using keysym_t = unsigned long;
using modmask_t = unsigned int;
using modded_sym = std::pair< modmask_t, keysym_t >; // mod, sym

// More readable aliases for common modifiers
constexpr const modmask_t AltMask = Mod1Mask;
constexpr const modmask_t SuperMask = Mod4Mask;

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * CONFIGURATION BEGIN                                                     *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Everything with Control or Alt is grabbed and disabled by default.
// Only the following hotkeys with Control or Alt are enabled:
const std::set< modded_sym > allowed = {
    { ControlMask, XK_a },
    { ControlMask, XK_c },
    { ControlMask, XK_v },
    { ControlMask, XK_x },
    { ControlMask, XK_z },
    { ControlMask, XK_f },
    { ControlMask, XK_g },
    { ControlMask, XK_BackSpace },
    { ControlMask, XK_Delete },
    { ControlMask, XK_Left },
    { ControlMask, XK_Right },
    { ControlMask, XK_Up },
    { ControlMask, XK_Down },

    { ControlMask | ShiftMask, XK_g },
    { ControlMask | ShiftMask, XK_v },
    { ControlMask | ShiftMask, XK_z },
    { ControlMask | ShiftMask, XK_Left },
    { ControlMask | ShiftMask, XK_Right },
    { ControlMask | ShiftMask, XK_Up },
    { ControlMask | ShiftMask, XK_Down },

    // Ctrl+Shift+{key} doesn't work in Firefox, unless Ctrl+Shift is enabled
    { ControlMask, XK_Shift_L },
    { ControlMask, XK_Shift_R },

    // Page zoom
    { ControlMask, XK_plus },
    { ControlMask, XK_minus },
    { ControlMask, XK_0 },
    { ControlMask, XK_eacute }, // 0
    { ControlMask, XK_equal },  // +
    { ControlMask | ShiftMask, XK_equal },  // +
    { ControlMask | ShiftMask, XK_1 },      // +
    { ControlMask | ShiftMask, XK_eacute }, // 0

    // Quit
    { AltMask, XK_F4 },
};

// Extra keys to grab and disable (with any modifier combination).
// Note that 'allowed' takes precedence.
const std::set< keysym_t > disabled_keys = {
    XK_F1,
    XK_F2,
    //XK_F3 is page search; safe
    XK_F4,
    XK_F5,
    XK_F6,
    //XK_F7 is caret browsing; probably safe
    XK_F8,
    XK_F9,
    XK_F10,
    XK_F11,
    XK_F12,
    XK_Menu,
};

// Switch layouts with Super+space
constexpr const modded_sym switch_hotkey = { SuperMask, XK_space };

// Keyboard layouts to cycle through
const std::vector< std::string > layouts = {
    "cz",
    "us",
    "sk",
    "cz dvorak-ucw",
};

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * CONFIGURATION END                                                       *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */


// The modifiers we are generally interested in. X11 uses the notion of a
// modifier also for things like CapsLock and NumLock, which we don't consider
// when matching keyboard shortcuts.
constexpr const modmask_t ShiftCtrlAltSuperMask = ShiftMask | ControlMask | AltMask | SuperMask;

bool is_switch_hotkey( XKeyEvent ev )
{
    return ( ev.state & ShiftCtrlAltSuperMask ) == switch_hotkey.first
        && XLookupKeysym( &ev, 0 ) == switch_hotkey.second;
}

bool key_is_allowed( XKeyEvent ev )
{
    /* For some reason, we sometimes get even keys we didn't grab which do not
     * have any of the disabled modifiers active.
     * Eg. "+ctrl +shift -ctrl +a -a -shift" sends 'A' (a + state 1, i.e. only
     * Shift) which we still get. Theferofe we green-light everything without
     * Ctrl or Alt, unless it is a disabled key. */
    KeySym ks = XLookupKeysym( &ev, 0 );
    if ( ( ev.state & ( AltMask | ControlMask ) ) == 0 && !disabled_keys.count(ks) )
        return true;

    return allowed.count({ ShiftCtrlAltSuperMask & ev.state, ks });
}

// Deny mouse button with certain modifiers
bool button_is_allowed( XButtonEvent ev )
{
    // Only consider the "standard" modificator keys
    ev.state &= ShiftCtrlAltSuperMask;
    if ( ev.button == Button4 || ev.button == Button5 ) // Wheel only without Alt
        return !( ev.state & AltMask ) ;
    if ( ev.button == Button3 ) // Right button never
        return false;
    return ev.state == 0; // Otherwise (left and middle) only unmodified
}

// Use setxkbmap to cycle keyboard layouts
void switch_layout()
{
    static int layout_index = 0;

    if ( layouts.empty() )
        return;

    const auto & next_layout = layouts[ layout_index ];
    ++layout_index;
    layout_index %= layouts.size();

    auto exitval = std::system( ("/usr/bin/setxkbmap " + next_layout).c_str() );
    if ( WEXITSTATUS( exitval ) != 0 )
        std::cerr << "failed to switch layout to " << next_layout << std::endl;
}

// Set passive key/button grabs
void regrab( Display *dpy, Window root ) {
    XUngrabKey( dpy, AnyKey, AnyModifier, root );
    XUngrabButton( dpy, AnyButton, AnyModifier, root );

    // Note that we also need to cover "boring" modifiers like CapsLock and NumLock
    for ( int mask = 1; mask < (1 << 8); ++mask ) {
        // Grab every modifier combination that contains Control or Alt
        if ( mask & ( ControlMask | AltMask ) )
            XGrabKey( dpy, AnyKey, mask, root, true, GrabModeAsync, GrabModeSync );

        // Layout switch hotkey
        else if ( ( mask & ShiftCtrlAltSuperMask ) == switch_hotkey.first )
            XGrabKey( dpy, XKeysymToKeycode( dpy, switch_hotkey.second ), mask,
                      root, true, GrabModeAsync, GrabModeSync );
    }

    // Grab additional disabled keys
    for ( auto ks : disabled_keys )
        XGrabKey( dpy, XKeysymToKeycode( dpy, ks ), AnyModifier,
                root, true, GrabModeAsync, GrabModeSync );

    // Mouse buttons
    XGrabButton( dpy, AnyButton, AnyModifier, root, true,
                 ButtonPressMask | ButtonReleaseMask, GrabModeSync, GrabModeAsync, None, None );
}

int main()
{
    Display * const dpy = XOpenDisplay( nullptr );
    if ( !dpy ) {
        std::cerr << "Unable to open display: " << XDisplayName( nullptr );
        return 1;
    }

    auto const screen = XDefaultScreen( dpy );
    auto const root = XRootWindow( dpy, screen );

    switch_layout();
    regrab( dpy, root );

    while ( true ) {
        XEvent event;
        XNextEvent( dpy, &event );

        bool enabled = true; // whether to pass the event to other windows

        switch ( event.type ) {
            case KeyRelease:
                if ( is_switch_hotkey( event.xkey ) )
                    switch_layout();
                [[fallthrough]];
            case KeyPress:
                enabled = !is_switch_hotkey( event.xkey ) && key_is_allowed( event.xkey );
                XAllowEvents( dpy, enabled ? ReplayKeyboard : AsyncKeyboard, event.xkey.time );
                break;

            case ButtonPress:
            case ButtonRelease:
                enabled = button_is_allowed( event.xbutton );
                XAllowEvents( dpy, enabled ? ReplayPointer : AsyncPointer, event.xbutton.time );
                break;

            // Keyboard layout change
            case MappingNotify:
                XRefreshKeyboardMapping( &event.xmapping );
                /* Regrabbing is currently not necessary, as the keys that we
                 * grab by name do not change their keysym <-> keycode mapping
                 * (e.g., space and F1 are still on the same keys).
                 *
                 * Commented out, as layout switching generates quite a lot
                 * MappingNotify events.
                 */
                //regrab( dpy, root );
                break;
        }

    }

    return 0;
}
