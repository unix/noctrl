# noctrl

I could not find a suitable daemon to complement a kiosk browser to block
certain keyboard shortcuts, so I wrote my own. No fancy configuration files,
just change the constants in the source code if the general idea seems useful
to you.

## Features

- **Block** (almost) all **shortcuts with Control or Alt** to disable browser
  functionality like editing the address bar, opening the developer's console,
  creating new tabs and switching to them etc.
- **White-list *some* of the shortcuts** like clipboard operations, caret
  navigation, text editing.
- **Block certain keys entirely** – mostly F-keys.
- **Disable mouse clicks with modifiers** as those can expose some browser
  functionality unwelcome in the kiosk.
- **Switch keyboard layouts** by means of a `setxkbmap` invocation. Software
  apparently does not handle shortcuts well when multiple xkb layouts (“groups”)
  are in play.

The daemon works by registering a lot of passive key grabs on the root X window.
Therefore, it probably will not work well within a desktop environment/window
manager that does the same. It is designed to run in an X session consisting of
just this daemon and a browser window.

## Dependencies

- `libX11`
- A C++14 compiler
